FROM alpine:3.9

LABEL "name"="Git-Crypt Unlock"
LABEL "maintainer"="Zemuldo <danstan.otieno@gmail.com>"
LABEL "maintainer"="Fuzail Sarang <fuzail@fingerprintsoft.org>"
LABEL "version"="0.0.0"

RUN apk add --update --no-cache bash

RUN apk --update add ca-certificates bash curl git g++ gnupg make openssh openssl openssl-dev && rm -rf /var/cache/apk/*

RUN curl -L https://github.com/AGWA/git-crypt/archive/debian/0.6.0-1.tar.gz | tar zxv -C /var/tmp

RUN cd /var/tmp/git-crypt-debian-0.6.0-1 && make && make install PREFIX=/usr/local && rm -rf /var/tmp/git-crypt

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
