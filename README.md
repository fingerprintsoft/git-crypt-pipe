# Bitbucket Pipelines Pipe: Git-Crypt Unlock

Allow unlocking of files encrypted by git-crypt in bitbucket pipelines.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: fingerprintsoft/git-crypt-pipe:0.1.0
    variables:
      GPG_PRIVATE_KEY: "<string>"
      GPG_KEY_GRIP: "<string>"
      GPG_KEY_PASS: "<string>"
      # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| GPG_PRIVATE_KEY (*)   | The sub key-grip |
| GPG_KEY_GRIP (*)      | Base64 encoded version of the private key |
| GPG_KEY_PASS (*)      | Key passphrase |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

[Here is how to get the values of the required secrets](SETUP_KEYS.md)
Store these as secrets in your bibucket repository

## Examples

Basic example:

```yaml
script:
  - pipe: fingerprintsoft/git-crypt-pipe:0.1.0
    variables:
      GPG_PRIVATE_KEY: ${GPG_PRIVATE_KEY}
      GPG_KEY_GRIP: ${GPG_KEY_GRIP}
      GPG_KEY_PASS: ${GPG_KEY_PASS}
```

Advanced example:

```yaml
script:
  - pipe: fingerprintsoft/git-crypt-pipe:0.1.0
    variables:
      GPG_PRIVATE_KEY: ${GPG_PRIVATE_KEY}
      GPG_KEY_GRIP: ${GPG_KEY_GRIP}
      GPG_KEY_PASS: ${GPG_KEY_PASS}
      DEBUG: "true"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by fuzail@fingerprintsoft.org.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
