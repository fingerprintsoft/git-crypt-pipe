#!/usr/bin/env bash
#
# Allow unlocking of files encrypted by git-crypt in bitbucket pipelines.
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
GPG_PRIVATE_KEY=${GPG_PRIVATE_KEY:?'GPG_PRIVATE_KEY variable missing.'}
GPG_KEY_PASS=${GPG_KEY_PASS:?'GPG_KEY_PASS variable missing.'}
GPG_KEY_GRIP=${GPG_KEY_GRIP:?'GPG_KEY_GRIP variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}

echo "$GPG_PRIVATE_KEY" | base64 -d > "$HOME"/git-crypt-key.asc

gpg --batch --import "$HOME"/git-crypt-key.asc

gpgconf --kill gpg-agent

gpg-agent --daemon --allow-preset-passphrase --max-cache-ttl 3153600000

/usr/libexec/gpg-preset-passphrase --preset --passphrase "$GPG_KEY_PASS" "$GPG_KEY_GRIP"

git-crypt unlock

if [[ $? -eq 0 ]]; then
  success "Success!"
else
  fail "Error!"
fi

rm "$HOME"/git-crypt-key.asc


