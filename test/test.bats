#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/git-crypt-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

load env-vars

@test "check env vars" {
    run : ${GPG_KEY_GRIP?"Need to set GPG_KEY_GRIP"}
    run : ${GPG_PRIVATE_KEY?"Need to set GPG_PRIVATE_KEY"}
    run : ${GPG_KEY_PASS?"Need to set GPG_KEY_PASS"}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Dummy test" {
    run docker run \
        -e GPG_KEY_GRIP="${GPG_KEY_GRIP}" \
        -e GPG_PRIVATE_KEY="${GPG_PRIVATE_KEY}" \
        -e GPG_KEY_PASS="${GPG_KEY_PASS}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

